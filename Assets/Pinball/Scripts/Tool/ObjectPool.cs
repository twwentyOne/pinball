﻿using UnityEngine;
using System.Collections.Generic;
 
public class ObjectPool : MonoBehaviour
{
	public static ObjectPool Instance;
    [SerializeField] private GameObject _objectPrefabs;
 	[SerializeField] private int _amountToBuffer;
	
    private List<GameObject> _pooledObjects;
   
    private GameObject containerObject;
   
    private void Awake ()
    {
        Instance = this;
    }
   
    private void Start ()
    {	
		containerObject = new GameObject("ObjectPool");
		
        _pooledObjects = new List<GameObject>();
		for( int i = 0; i < _amountToBuffer; i++ )
		{
			GameObject obj = Instantiate(_objectPrefabs);
			obj.transform.parent = containerObject.transform;
			obj.SetActive(false);
			_pooledObjects.Add(obj);
		}
    }

	public GameObject GetPooledObject()
	{
		for( int i = 0; i < _pooledObjects.Count; i++ )
		{
			if(!_pooledObjects[i].activeInHierarchy)
			{
				return _pooledObjects[i];
			}
		}
		return null;
	}
}