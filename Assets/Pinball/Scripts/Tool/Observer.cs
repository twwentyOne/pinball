﻿using System;

public class Observer<T>
{
	private T _currentValue;

    public Observer(T initialValue)
    {
		 _currentValue = initialValue;
    }

    public T Value
    {
        get { return _currentValue; }
        set
        {
            if (_currentValue.Equals(value)) return;
            _currentValue = value;
            var args = new ChangedValueArgs<T>(_currentValue);

            if (OnValueChanged != null) OnValueChanged(this, args);
        }
    }

    public event EventHandler<ChangedValueArgs<T>> OnValueChanged;

    public void ChangeValue(T value)
    {
        _currentValue = value;
    }
}

public class ChangedValueArgs<T> : EventArgs
{
	public T Value { get; private set; }

    public ChangedValueArgs(T value)
    {
		Value = value;
    }
}