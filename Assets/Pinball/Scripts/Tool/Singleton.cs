﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component
{
    private static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance != null) return _instance;
            _instance = FindObjectOfType<T>();
            if (_instance != null) return _instance;
            var obj = new GameObject();
            _instance = obj.AddComponent<T>();
            return _instance;
        }
    }
 
    private void Awake()
    {
        _instance = this as T;
        OnCreated();
    }
 
    private void OnDestroy()
    {
        _instance = null;
        OnDestroyed();
    }
 
    public virtual void OnCreated()
    {
    }
    
    public virtual void OnDestroyed()
    {   
    }
}