﻿using UnityEngine;

[RequireComponent(typeof(ObjectPool))]
public class Respawn : MonoBehaviour 
{
	private ObjectPool _pooledObjects;

	private void Start()
	{
		_pooledObjects = GetComponent<ObjectPool>();
	}

	private void Update()
	{
		GameObject obj = _pooledObjects.GetPooledObject();
		if(obj == null) return;
		obj.transform.position = this.transform.position;
		obj.SetActive(true);
	}
}