﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{	
	[SerializeField] private int _bonus;
	private int _ballCount;

	private void OnTriggerEnter2D(Collider2D collision)
	{	
		if(collision.name != "Ball(Clone)") return;
		GameManager.Instance.UpdateBallCount(1);
		collision.gameObject.SetActive(false);
		
		_ballCount++;
		if(_ballCount >= 3)
        {
           GameManager.Instance.EndLevel();
        };
    }

    private void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.name != "Ball(Clone)") return;
		GameManager.Instance.UpdateScore(_bonus);
	}
}