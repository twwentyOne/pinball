﻿using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    Home,
    Game
}

public class GameManager: Singleton<GameManager>
{
    [SerializeField] private HomeScreen _homeScreen;
    [SerializeField] private GameScreen _gameScreen;
    public Observer<int> ScoreObserver;
    public Observer<int> BallObserver;

    public StateMachine<GameState> StateMachine;

    public override void OnCreated()
    {   
        ScoreObserver = new Observer<int>(0);   
        BallObserver = new Observer<int>(0);
        
        StateMachine = new StateMachine<GameState>();
        StateMachine.AddState(GameState.Home, _homeScreen.StartScreen, _homeScreen.StopScreen);
        StateMachine.AddState(GameState.Game, _gameScreen.StartScreen, _gameScreen.StopScreen);
    }

    private void Start()
    {
        StateMachine.SetState(GameState.Home);
        _homeScreen.StartButton += () => StartLevel();
    }

    private void StartLevel()
    {
        StateMachine.SetState(GameState.Game);
        ScoreObserver.Value = 0;
        BallObserver.Value = 3;
    }

    public void EndLevel()
    {
        ScoreManager.SaveScore(ScoreObserver.Value);
        SceneManager.LoadScene("Main");
    }

    public void UpdateScore(int score)
    {
        ScoreObserver.Value += score;
    }

    public void UpdateBallCount(int count)
    {
        BallObserver.Value -= count;
    }
}