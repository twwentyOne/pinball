﻿using UnityEngine;

public class Flipper : MonoBehaviour 
{ 
   [SerializeField] private GameObject leftFlipper;
   [SerializeField] private GameObject rightFlipper;
   private Rigidbody2D _rbLeftFlipper;
   private Rigidbody2D _rbRightFlipper;
   private float _torgueForce = 700f;
   
    private void Start()
   {	
	   _rbLeftFlipper = leftFlipper.GetComponent<Rigidbody2D>();
	   _rbRightFlipper = rightFlipper.GetComponent<Rigidbody2D>();
   }

   private void Update()
   {
	   FlipLeft();
	   FlipRight();
   }
   
   private void FlipLeft()
   {
	   if(Input.GetKeyDown(KeyCode.Z))
		{  
			AddTorque(_rbLeftFlipper, _torgueForce);
		}
   }

   private void FlipRight()
   {	
	   if(Input.GetKeyDown(KeyCode.X))
	   {
		   AddTorque(_rbRightFlipper, - _torgueForce);
	   }
   }

   private void AddTorque(Rigidbody2D rigit, float force)
   {
	    rigit.AddTorque(force);
   } 
}