﻿using UnityEngine;
 
public class Launcher : MonoBehaviour 
{
   private SliderJoint2D[] _sliderJoint;
   private JointMotor2D _jointMotor;

   private void Start()
   {
       _sliderJoint = gameObject.GetComponents<SliderJoint2D>();
       _jointMotor = _sliderJoint[0].motor;
   }
 
   private void Update()
   {
       Plunger();
   }

   private void Plunger()
   {
		if (Input.GetKeyDown(KeyCode.Space))
		{	
			_jointMotor.motorSpeed = -80;
		}
		else if (Input.GetKeyUp(KeyCode.Space))
		{
			_jointMotor.motorSpeed = 80;
		}
		else
		{
			_jointMotor.motorSpeed = 0;
		} 
		_sliderJoint[0].motor = _jointMotor;
   }
}