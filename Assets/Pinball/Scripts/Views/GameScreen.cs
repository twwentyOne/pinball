﻿using UnityEngine;
using UnityEngine.UI;

public class GameScreen : MonoBehaviour
{
    [Header("Texts")]
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _ballCountText;

    public void StartScreen()
    {
        GameManager.Instance.ScoreObserver.OnValueChanged += SetScoreText;   
        GameManager.Instance.BallObserver.OnValueChanged += SetBallCountText;
        gameObject.SetActive(true);
    }

    public void StopScreen()
    {
        GameManager.Instance.ScoreObserver.OnValueChanged -= SetScoreText; 
        GameManager.Instance.BallObserver.OnValueChanged -= SetBallCountText;
        gameObject.SetActive(false);
    }

    private void SetScoreText(object sender, ChangedValueArgs<int> args)
    {
        _scoreText.text = args.Value.ToString("000000");   
    }

    private void SetBallCountText(object sender, ChangedValueArgs<int> args)
    {
        _ballCountText.text = args.Value.ToString("Ball - " + "0");
    }
}