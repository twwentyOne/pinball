﻿using UnityEngine.UI;
using UnityEngine;
using System;

public class HomeScreen : MonoBehaviour 
{	
	[Header("Buttons")]
	[SerializeField] private Button _startButton;
	
	[Header("Texts")]
    [SerializeField] private Text _bestScoreText;
    [SerializeField] private Text _lastScoreText;

	public event Action StartButton;
	
	public void StartScreen()
	{
		_startButton.onClick.AddListener(()=>
		{
			if(StartButton != null) StartButton();
		});
		SetScoreText();
		ScreenGoIn();
	}

	private void ScreenGoIn()
	{
    	gameObject.SetActive(true);
    }

	public void StopScreen()
    {
        _startButton.onClick.RemoveAllListeners();
		gameObject.SetActive(false);
	}

	private void SetScoreText()
	{
		_bestScoreText.text = string.Format("Best: {0}", ScoreManager.GetBestScore());
		_lastScoreText.text = string.Format("Last: {0}", ScoreManager.GetLastScore());
	}
}