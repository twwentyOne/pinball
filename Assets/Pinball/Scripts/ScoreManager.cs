﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
	private static string _bestScoreTag = "BEST_SCORE";
    private static string _lastScoreTag = "LAST_SCORE";
	
	public static void SaveScore(int lastScore)
    {
		PlayerPrefs.SetInt(_lastScoreTag, lastScore);
        var bestScore = GetBestScore();
        if (lastScore > bestScore)
        {
			PlayerPrefs.SetInt(_bestScoreTag, lastScore);
        }
    }
	
	public static int GetBestScore()
    {   
	    return PlayerPrefs.GetInt(_bestScoreTag);
    }

	public static int GetLastScore()
	{
		return PlayerPrefs.GetInt(_lastScoreTag);
    }
}